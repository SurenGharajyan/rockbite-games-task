package com.suren.company.main;

import com.suren.company.main.component.enums.MaterialTypes;
import com.suren.company.main.component.model.SandShip;
import com.suren.company.main.component.model.Warehouse;
import com.suren.company.main.component.model.materials.Copper;
import com.suren.company.main.component.model.materials.Iron;
import com.suren.company.main.component.model.materials.Material;
import com.suren.company.main.component.model.materials.UnknownMaterial;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private SandShip sandShip;

    public static void main(String[] args) {
        Main.initialize();
    }


    public static void initialize() {
        Main main = new Main();
        List<Warehouse> warehouseList = main.initWarehouses(3);
        main.sandShip = new SandShip(warehouseList);
    }

    private List<Warehouse> initWarehouses(int count) {
        List<Warehouse> warehouses = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            int quantityOfWarehouse = i * 1000;
            List<Material> currMaterials = this.initMaterials(count, quantityOfWarehouse);
            warehouses.add(new Warehouse("Warehouse #" + i, i, currMaterials, quantityOfWarehouse));
        }
        return warehouses;
    }


    private List<Material> initMaterials(int countOfMaterials, int warehouseQuantity) {
        int sumQuantity = 0;
        List<Material> materialsList = new ArrayList<>();
        Material createMaterial;
        for (int i = 1; i <= countOfMaterials; i++) {
            if (sumQuantity >= warehouseQuantity) {
                break;
            }
            int quantityOfWarehouse = (int) (Math.random() * 100);
            MaterialTypes mType =
                    MaterialTypes.values()[
                            Math.min((int) (Math.random() * 10), MaterialTypes.values().length - 1)
                            ];
            sumQuantity += quantityOfWarehouse;

            switch (mType) {
                case IRON:
                    createMaterial = new Iron(
                            "Iron Material " + i,
                            "Some Iron Description",
                            "icon",
                            quantityOfWarehouse);
                    break;
                case COPPER:
                    createMaterial = new Copper(
                            "Copper Material " + i,
                            "Some Copper Description",
                            "icon",
                            quantityOfWarehouse);
                    break;
                //                    ...
                //                    ...
                //                    ...
                //                    ...
                default:
                    createMaterial = new UnknownMaterial(
                            "Unknown Material " + i,
                            "Some Unknown Description",
                            "icon",
                            quantityOfWarehouse);
            }
            materialsList.add(createMaterial);
        }
        return materialsList;
    }
}
