package com.suren.company.main.component.enums;

public enum MaterialTypes {
    IRON, COPPER, BOLT, STEEL, SILICON, SULFUR
}
