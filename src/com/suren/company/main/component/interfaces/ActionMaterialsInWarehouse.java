package com.suren.company.main.component.interfaces;

import com.suren.company.main.component.model.SandShip;
import com.suren.company.main.component.model.materials.Material;

public interface ActionMaterialsInWarehouse {
    Material addMaterialToWarehouse(Material currentMaterial);
    Material removeMaterialToWarehouse(Material removeCurrentMaterial);
    Material moveToOtherWarehouse(Material selectedMaterial, int wareHouseIndex, SandShip sandShip);
}
