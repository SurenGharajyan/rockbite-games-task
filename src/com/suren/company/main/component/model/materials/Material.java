package com.suren.company.main.component.model.materials;

public class Material {
    private int id;
    private String name;
    private String description;
    private String icon;
    private int capacity;


    public Material(String name, String description, String icon, int capacity) {
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.capacity = capacity;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }
}
