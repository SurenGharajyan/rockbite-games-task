package com.suren.company.main.component.model.materials;

public class Bolt extends Material {

    public Bolt(String name, String description, String icon, int capacity) {
        super(name, description, icon, capacity);
    }
}
