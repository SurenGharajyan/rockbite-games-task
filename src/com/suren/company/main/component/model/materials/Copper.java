package com.suren.company.main.component.model.materials;

public class Copper extends Material {

    public Copper(String name, String description, String icon, int capacity) {
        super(name, description, icon, capacity);
    }
}
