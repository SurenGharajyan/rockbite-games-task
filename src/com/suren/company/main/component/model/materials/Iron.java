package com.suren.company.main.component.model.materials;

public class Iron extends Material {

    public Iron(String name, String description, String icon, int capacity) {
        super(name, description, icon, capacity);
    }
}
