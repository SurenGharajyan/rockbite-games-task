package com.suren.company.main.component.model.materials;

public class UnknownMaterial extends Material {

    public UnknownMaterial(String name, String description, String icon, int capacity) {
        super(name, description, icon, capacity);
    }
}
