package com.suren.company.main.component.model;

import java.util.List;

public class SandShip {
    private List<Warehouse> warehouseList;

    public SandShip(List<Warehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public List<Warehouse> getWarehouseList() {
        return warehouseList;
    }
}
