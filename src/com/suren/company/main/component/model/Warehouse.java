package com.suren.company.main.component.model;

import com.suren.company.main.component.interfaces.ActionMaterialsInWarehouse;
import com.suren.company.main.component.model.materials.Material;

import java.util.List;

public class Warehouse implements ActionMaterialsInWarehouse {
    private int id;
    private int index;
    private List<Material> materials;
    private int quantities;


    public Warehouse(String title, int index, List<Material> materials, int quantities) {
        this.index = index;
        this.id = hashCode();
        this.materials = materials;
        this.quantities = quantities;
    }


    public int getQuantities() {
        return quantities;
    }

    public List<Material> getMaterials() {
        return materials;
    }


    public int getId() {
        return id;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public Material addMaterialToWarehouse(Material currentMaterial) {
        currentMaterial.setId(currentMaterial.hashCode());
        materials.add(currentMaterial);
        return currentMaterial;
    }

    @Override
    public Material removeMaterialToWarehouse(Material selectedMaterial) {
        materials.remove(selectedMaterial);
        return null;
    }

    @Override
    public Material moveToOtherWarehouse(Material selectedMaterial, int warehouseIndex, SandShip sandShip) {
        Warehouse moveToWarehouse = sandShip.getWarehouseList().get(warehouseIndex);
        moveToWarehouse.addMaterialToWarehouse(selectedMaterial);
        this.removeMaterialToWarehouse(selectedMaterial);
        return null;
    }

}
